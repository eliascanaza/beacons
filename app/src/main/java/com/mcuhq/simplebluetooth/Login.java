package com.mcuhq.simplebluetooth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mcuhq.simplebluetooth.utils.Constants;
import com.mcuhq.simplebluetooth.utils.HttpHandler;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by emamani on 13/09/2017.
 */


public class Login extends AppCompatActivity {

    private AppCompatButton btnLogin;
    private EditText etDNI;
    private String url_customer_type = "";
    private String TAG = Login.class.getName();
    private String DB = "myDB_Promo";

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        // Obtenemos valores de memoria interna
        SharedPreferences settings = getSharedPreferences(DB, 0);
        String strTipoSegmento = settings.getString(Constants.TIPO_SEGMENTO, "");
        String strBanco = settings.getString(Constants.CLIENTE_BANCO, "");

        if(!strTipoSegmento.isEmpty()) {
            Intent myIntent = new Intent(Login.this, MainActivity.class);
            Login.this.startActivity(myIntent);
        }else{
            btnLogin = (AppCompatButton)findViewById(R.id.btn_login);
            etDNI = (EditText)findViewById(R.id.dni_user);

            mActivity = Login.this;

            url_customer_type = Constants.URL_GET_CUSTOMER_TYPE;

            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            url_customer_type = url_customer_type.replace(Constants.COD_DOCUMENTO, etDNI.getText().toString() );

                            new GetCustomerType().execute((Void[])null);
                        }
                    });

                }
            });
        }
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetCustomerType extends AsyncTask<Void, Void, String> {
        private ProgressDialog progress;
        private JSONObject jsonObject;
        private String strSegmentoCliente = "";
        private String strTipoFidelizacion = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progress = new ProgressDialog(Login.this);
            progress.setTitle(getString(R.string.app_name_customer));
            progress.setMessage("consultando...");
            progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
            progress.show();
        }

        @Override
        protected String doInBackground(Void... url) {
            HttpHandler sh = new HttpHandler(getApplicationContext());
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url_customer_type);

            Log.e("responseLogin", "Response URL CustomerType [EM]: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String jsonStr) {
            super.onPostExecute(jsonStr);

            if (jsonStr != null) {
                try {
                    jsonObject = new JSONObject(jsonStr);
                    strSegmentoCliente = jsonObject.getString("tipoFide");
                    strTipoFidelizacion = jsonObject.getString("tipoSegmen");

                    //GUardamos en memoria los datos del usuario
                    SharedPreferences settings = getSharedPreferences(DB, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString(Constants.TIPO_FIDELIZACION, strSegmentoCliente);
                    editor.putString(Constants.TIPO_SEGMENTO, strTipoFidelizacion);
                    editor.putString(Constants.CLIENTE_BANCO, "INDRA");

                    editor.apply();

                    progress.dismiss();

                    Intent myIntent = new Intent(Login.this, MainActivity.class);
                    Login.this.startActivity(myIntent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Error en la respuestas TIPO CLIENTE, REST API",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }
        }

    }
}
