package com.mcuhq.simplebluetooth;

import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.estimote.coresdk.common.requirements.SystemRequirementsChecker;
import com.estimote.coresdk.observation.region.Region;
import com.estimote.coresdk.observation.region.beacon.BeaconRegion;
import com.estimote.coresdk.service.BeaconManager;
import com.mcuhq.simplebluetooth.estimote.estimote.EstimoteCloudBeaconDetails;
import com.mcuhq.simplebluetooth.estimote.estimote.EstimoteCloudBeaconDetailsFactory;
import com.mcuhq.simplebluetooth.estimote.estimote.ProximityContentManager;
import com.mcuhq.simplebluetooth.utils.Constants;
import com.mcuhq.simplebluetooth.utils.HttpHandler;
import com.mcuhq.simplebluetooth.utils.Notification;
import com.mcuhq.simplebluetooth.utils.ScanerBluetoothService;

import org.intellij.lang.annotations.Identifier;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Arrays;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private BeaconManager beaconManager;
    private static final String TAG = "MainActivity";

    // GUI Components
    private TextView tipoCliente;
    private TextView banco;

    private String DB = Constants.DB_NAME;

    private ProximityContentManager proximityContentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_customer);

        // Obtenemos valores de memoria interna
        SharedPreferences settings = getSharedPreferences(DB, 0);
        String strTipoSegmento = settings.getString(Constants.TIPO_SEGMENTO, "");
        String strBanco = settings.getString(Constants.CLIENTE_BANCO, "");

        tipoCliente = (TextView)findViewById(R.id.tipoCliente);
        tipoCliente.setText(strTipoSegmento);

        banco = (TextView)findViewById(R.id.banco);
        banco.setText(strBanco);

        Log.e("BEACONNIE","EMPEZOO-111122222");

    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.e("BEACONNIE","EMPEZOO-1111");

        MyApplication app = (MyApplication) getApplication();

        if (!SystemRequirementsChecker.checkWithDefaultDialogs(this)) {
            Log.e(TAG, "Can't scan for beacons, some pre-conditions were not met");
            Log.e(TAG, "Read more about what's required at: http://estimote.github.io/Android-SDK/JavaDocs/com/estimote/sdk/SystemRequirementsChecker.html");
            Log.e(TAG, "If this is fixable, you should see a popup on the app's screen right now, asking to enable what's necessary");

        } else {
            Log.d(TAG, "Enabling beacon notifications");
            app.enableBeaconNotifications();
        }
    }
}
