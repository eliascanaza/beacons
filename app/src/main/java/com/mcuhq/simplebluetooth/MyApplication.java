package com.mcuhq.simplebluetooth;

import android.app.Application;
import android.util.Log;

import com.estimote.coresdk.common.config.EstimoteSDK;
import com.estimote.coresdk.observation.region.beacon.BeaconRegion;
import com.estimote.coresdk.recognition.packets.Beacon;
import com.estimote.coresdk.recognition.packets.ConfigurableDevice;
import com.estimote.coresdk.service.BeaconManager;
import com.mcuhq.simplebluetooth.estimote.BeaconNotificationsManager;
import com.mcuhq.simplebluetooth.estimote.estimote.ProximityContentManager;
import com.mcuhq.simplebluetooth.utils.ValidateBeacon;

import java.util.List;
import java.util.UUID;

//
// Running into any issues? Drop us an email to: contact@estimote.com
//

public class MyApplication extends Application {

    private boolean beaconNotificationsEnabled = false;

    private BeaconManager beaconManager;
    private ValidateBeacon validateBeacon;

    @Override
    public void onCreate() {
        super.onCreate();

        // TODO: put your App ID and App Token here
        // You can get them by adding your app on https://cloud.estimote.com/#/apps
        EstimoteSDK.initialize(getApplicationContext(), "notification-indra-h71", "f8a46e3f1bc1b196231fb7548ef35975");

        // uncomment to enable debug-level logging
        // it's usually only a good idea when troubleshooting issues with the Estimote SDK
        //EstimoteSDK.enableDebugLogging(true);

        validateBeacon = new ValidateBeacon(getBaseContext());
    }

    public void enableBeaconNotifications() {
        final BeaconManager beaconManager = new BeaconManager(this);
        // set foreground scan periods. This one will scan for 2s and wait 2s
        // connects beacon manager to underlying service
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                // add listener for ConfigurableDevice objects

                beaconManager.setMonitoringListener(new BeaconManager.BeaconMonitoringListener() {
                    @Override
                    public void onEnteredRegion(BeaconRegion beaconRegion, List<Beacon> beacons) {
                        //Log.e("BEACONNN-aa", beacons.toString());
                        //Log.e("BEACONNN-aaa", beacons.get(0)+"***");
                        if(beacons.size() > 0)
                            validateBeacon.ExecuteService(beacons.get(0).getProximityUUID().toString().toUpperCase(), beaconRegion.getIdentifier());
                    }

                    @Override
                    public void onExitedRegion(BeaconRegion beaconRegion) {
                        //Log.e("BEACONNN", "------");
                    }
                });
                beaconManager.startMonitoring(new BeaconRegion("Rosado",UUID.fromString("89F26904-95DE-1073-C2CD-EB80C079B2CF"),null,null));
                beaconManager.startMonitoring(new BeaconRegion("Amarillo",UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),null,null));
                beaconManager.startMonitoring(new BeaconRegion("Morado",UUID.fromString("96E9FEFB-7BA3-D682-8939-EB1FAB6A1B16"),null,null));
            }
        });

    }
}
