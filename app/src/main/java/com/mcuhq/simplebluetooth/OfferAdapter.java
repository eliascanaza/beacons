package com.mcuhq.simplebluetooth;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mcuhq.simplebluetooth.model.Product;
import com.mcuhq.simplebluetooth.utils.Constants;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.util.List;

/**
 * Created by emamani on 12/09/2017.
 */

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.ProductViewHolder>{

    List<Product> products;
    String urlDownloadImage = Constants.URL_GET_IMAGEN;

    OfferAdapter(List<Product> persons){
        this.products = persons;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_offer, viewGroup, false);
        ProductViewHolder pvh = new ProductViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder productViewHolder, int position) {
        productViewHolder.productName.setText(products.get(position).getStrProductName());
        productViewHolder.discountProduct.setText(products.get(position).getStrDiscount());

        urlDownloadImage = urlDownloadImage.replace(Constants.COD_IMAGEN,products.get(position).getStrImagen());

        new DownloadImageTask(productViewHolder.productPhoto).execute(urlDownloadImage);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView productName;
        TextView discountProduct;
        ImageView productPhoto;

        ProductViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            productName = (TextView)itemView.findViewById(R.id.product_name);
            discountProduct = (TextView)itemView.findViewById(R.id.tvDiscount);
            productPhoto = (ImageView)itemView.findViewById(R.id.product_photo);
        }
    }

    private static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}
