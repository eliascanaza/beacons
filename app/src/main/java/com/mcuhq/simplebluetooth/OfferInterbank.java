package com.mcuhq.simplebluetooth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mcuhq.simplebluetooth.model.Product;
import com.mcuhq.simplebluetooth.utils.Constants;
import com.mcuhq.simplebluetooth.utils.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by emamani on 12/09/2017.
 */

public class OfferInterbank extends AppCompatActivity {

    private List<Product> products;

    private String TAG = OfferInterbank.class.getSimpleName();

    RecyclerView rv;
    private Intent myIntent;
    private String jsonStr;
    private String DB = "myDB_Promo";

    private SharedPreferences.Editor editor;
    private SharedPreferences settings;
    private String strLastBLE = "";
    private String URL_BEACON_LEIDO = "";

    private String strTipoSemento = "";
    private String strTipoFidelizacion = "";

    private Button btnQR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offer_products);

        btnQR = (Button)findViewById(R.id.btnGenerateQR);
        btnQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(OfferInterbank.this, QR_scanner.class);
                OfferInterbank.this.startActivity(myIntent);
            }
        });

        settings = getBaseContext().getSharedPreferences(Constants.DB_NAME, 0);

        strTipoSemento = settings.getString(Constants.TIPO_SEGMENTO, "");
        strTipoFidelizacion = settings.getString(Constants.TIPO_FIDELIZACION, "");

        /**
         * Actualizamos la red accedida
         * */

        settings = getApplicationContext().getSharedPreferences(DB, 0);
        editor = settings.edit();

        rv = (RecyclerView)findViewById(R.id.rv);

        myIntent = getIntent();
        jsonStr = myIntent.getStringExtra("jsonStr");

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(llm);

        ListOffer(jsonStr);

    }

    public void ListOffer(String jsonStr){
        products = new ArrayList<>();

        if (jsonStr != null) {
            try {
                JSONArray jsonArray;
                jsonArray = new JSONArray(jsonStr);

                for(int i = 0;i < jsonArray.length(); i++){
                    JSONObject c = jsonArray.getJSONObject(i);

                    String id = c.getString("id");
                    String desCategory = "";
                    String desProduct = c.getString("no_Establecimiento")+" - "+c.getString("de_Promocion");
                    String strImagen = c.getString("de_imagen");
                    String desDisc = c.getString("de_val_fidelidad");

                    strLastBLE = c.getString("beacon");

                    products.add(new Product(desProduct, desCategory, desDisc , id, strImagen));
                }

                editor.putString(Constants.LAST_RED_BLE, strLastBLE);
                editor.apply();

                //PROMOCION LEIDA
                URL_BEACON_LEIDO = Constants.URL_BEACON_LEIDO;

                URL_BEACON_LEIDO = URL_BEACON_LEIDO.replace(Constants.COD_BEACON, strLastBLE);
                URL_BEACON_LEIDO = URL_BEACON_LEIDO.replace(Constants.COD_PRODUCTO_FIDELIZACION, strTipoFidelizacion);
                URL_BEACON_LEIDO = URL_BEACON_LEIDO.replace(Constants.COD_SEGMENTO_CLIENTE, strTipoSemento);

                Log.e("URL-LEIDO", URL_BEACON_LEIDO);

                //HttpHandler sh = new HttpHandler(getApplicationContext());
                //sh.makeServiceCall(URL_BEACON_LEIDO);
                AsyncHttpClient client = new AsyncHttpClient();
                client.get(URL_BEACON_LEIDO, new AsyncHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        // called before request is started
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                        // called when response HTTP status is "200 OK"
                        Log.e("200","200");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                        Log.e("400","400");
                    }

                    @Override
                    public void onRetry(int retryNo) {
                        // called when request is retried
                        Log.e("00","00");
                    }
                });

            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(),
                        "ERROR JSON: "+e.getMessage(),
                        Toast.LENGTH_LONG)
                        .show();
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "Couldn't get json from server.");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            "Couldn't get json from server. Check LogCat for possible errors!",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });

        }

        OfferAdapter adapter = new OfferAdapter(products);
        rv.setAdapter(adapter);
    }
}
