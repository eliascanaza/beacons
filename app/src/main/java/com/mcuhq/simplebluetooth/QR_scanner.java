package com.mcuhq.simplebluetooth;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.mcuhq.simplebluetooth.utils.Constants;

/**
 * Created by eliascanaza on 18/10/17.
 */

public class QR_scanner extends Activity  {

    private ImageView imgQR;
    private TextView tvValue;
    private String strQRValue = "";
    private String strBeacon = "";
    private String strTipoSemento = "";
    private String strTipoFidelizacion = "";

    private SharedPreferences settings;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.qr_code_layout);

        settings = getBaseContext().getSharedPreferences(Constants.DB_NAME, 0);

        strTipoSemento = settings.getString(Constants.TIPO_SEGMENTO, "");
        strTipoFidelizacion = settings.getString(Constants.TIPO_FIDELIZACION, "");
        strBeacon = settings.getString(Constants.LAST_RED_BLE, "");

        strQRValue = strBeacon+"/"+strTipoFidelizacion+"/"+strTipoSemento;

        tvValue = (TextView)findViewById(R.id.tvValueQR);
        imgQR = (ImageView) findViewById(R.id.ivQRCode);

        tvValue.setText(strQRValue);

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();

        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(strQRValue, BarcodeFormat.QR_CODE, 200, 200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();

            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            imgQR.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

    }

}