package com.mcuhq.simplebluetooth;

import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.mcuhq.simplebluetooth.utils.Constants;
import com.mcuhq.simplebluetooth.utils.HttpHandler;
import com.mcuhq.simplebluetooth.utils.Notification;
import com.mcuhq.simplebluetooth.utils.ScanerBluetoothService;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by edgarcia on 13/09/2017.
 */

public class ScanerReceiver extends BroadcastReceiver {

    private String DB = "myDB_Promo";
    private String nameDevice = "";
    private String URL_OFFER = "";
    private String strTipoSemento = "";
    private String strTipoFidelizacion = "";
    private String last_red = "";

    private final String TAG = ScanerReceiver.class.getSimpleName();

    private SharedPreferences.Editor editor;
    private SharedPreferences settings;

    Context finalContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        finalContext = context;

        URL_OFFER = Constants.URL_GET_OFFER;

        // Obtenemos valores de memoria interna
        settings = finalContext.getSharedPreferences(DB, 0);
        editor = settings.edit();

        strTipoSemento = settings.getString(Constants.TIPO_SEGMENTO, "");
        strTipoFidelizacion = settings.getString(Constants.TIPO_FIDELIZACION, "");

        final String action = intent.getAction();


        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    if(isMyServiceRunning(ScanerBluetoothService.class)){
                        context.stopService(new Intent(context, ScanerBluetoothService.class));
                    }
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    editor.putString(Constants.LAST_RED_BLE, "");
                    editor.apply();
                    break;
                case BluetoothAdapter.STATE_ON:
                    //Empezamos el servicio
                    if(!isMyServiceRunning(ScanerBluetoothService.class)){
                        context.startService(new Intent(context, ScanerBluetoothService.class));
                    }

                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    editor.putString(Constants.LAST_RED_BLE, "");
                    editor.apply();

                    break;
            }
        }



        if(BluetoothDevice.ACTION_FOUND.equals(action)){
            last_red = settings.getString(Constants.LAST_RED_BLE, "");

            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if(device != null) {
                if(device.getName() != null && !device.getName().isEmpty()) {
                    nameDevice = device.getName().toLowerCase();

                    //If found the Interbank bloutooth get offers
                    if (nameDevice.contains(Constants.COD_BEACON_CONFIG) && (!nameDevice.equals(last_red))) {

                        //llamamos al servicio para obtener las ofertas
                        URL_OFFER = URL_OFFER.replace(Constants.COD_BEACON, nameDevice);
                        URL_OFFER = URL_OFFER.replace(Constants.COD_PRODUCTO_FIDELIZACION, strTipoFidelizacion);
                        URL_OFFER = URL_OFFER.replace(Constants.COD_SEGMENTO_CLIENTE, strTipoSemento);

                        new GetOfferProduct().execute();
                    }
                }
            }
        }


        if (action.equals(Intent.ACTION_SCREEN_OFF)) {
            Log.d("screenoffReceiver", "SCREEN OFF");
            // do whatever you need to do here
            context.startService(new Intent(context, ScanerBluetoothService.class));
        } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
            Log.d("screenoffReceiver", "SCREEN ON");
            // and do whatever you need to do here
            context.startService(new Intent(context, ScanerBluetoothService.class));
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) finalContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    /**
     * Async task class to get json by making HTTP call
     */
    private class GetOfferProduct extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... url) {
            HttpHandler sh = new HttpHandler(finalContext);
            Log.e("response-prmo", URL_OFFER+"-");
            return sh.makeServiceCall(URL_OFFER);
        }

        @Override
        protected void onPostExecute(String jsonStr) {
            super.onPostExecute(jsonStr);

            Log.e("response-prmo2", jsonStr+"-");

            if (jsonStr != null) {
                try {
                    JSONArray jsonArray;
                    jsonArray = new JSONArray(jsonStr);

                   // if (jsonArray.length() > 0)
                        //new Notification().Launch(finalContext, jsonStr);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
