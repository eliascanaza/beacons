package com.mcuhq.simplebluetooth.estimote;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.estimote.coresdk.recognition.packets.EstimoteLocation;
import com.estimote.coresdk.service.BeaconManager;
import com.estimote.monitoring.EstimoteMonitoring;
import com.estimote.monitoring.EstimoteMonitoringListener;
import com.estimote.monitoring.EstimoteMonitoringPacket;
import com.mcuhq.simplebluetooth.MainActivity;
import com.mcuhq.simplebluetooth.ScanerReceiver;
import com.mcuhq.simplebluetooth.utils.Constants;
import com.mcuhq.simplebluetooth.utils.HttpHandler;
import com.mcuhq.simplebluetooth.utils.Notification;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;
import java.util.Random;

public class BeaconNotificationsManager {

    private static final String TAG = "BeaconNotifications";

    private String URL_OFFER = "";
    private String strTipoSemento = "";
    private String strTipoFidelizacion = "";
    private String DB = Constants.DB_NAME;

    private SharedPreferences.Editor editor;
    private SharedPreferences settings;

    private BeaconManager beaconManager;
    private EstimoteMonitoring estimoteMonitoring;

    private String enterMessages;
    private String exitMessages;
    private String deviceIdBeacon;

    private Context context;

    private int notificationID = 0;

    public BeaconNotificationsManager(final Context context) {
        this.context = context;
        beaconManager = new BeaconManager(context);
        estimoteMonitoring = new EstimoteMonitoring();
        estimoteMonitoring.setEstimoteMonitoringListener(new EstimoteMonitoringListener() {
            @Override
            public void onEnteredRegion() {
                Log.e(TAG, "onEnteredRegion");
                String message = enterMessages;
                if (message != null) {
                    //llamamos al servicio para obtener las ofertas
                    URL_OFFER = URL_OFFER.replace(Constants.COD_BEACON, deviceIdBeacon);
                    URL_OFFER = URL_OFFER.replace(Constants.COD_PRODUCTO_FIDELIZACION, strTipoFidelizacion);
                    URL_OFFER = URL_OFFER.replace(Constants.COD_SEGMENTO_CLIENTE, strTipoSemento);
                    new GetOfferProduct().execute(deviceIdBeacon);
                }
            }

            @Override
            public void onExitedRegion() {
                Log.d(TAG, "onExitedRegion");
            }
        });

        beaconManager.setLocationListener(new BeaconManager.LocationListener() {
            @Override
            public void onLocationsFound(List<EstimoteLocation> locations) {
                for (EstimoteLocation location : locations) {
                    if (location.id.toHexString().equals(deviceIdBeacon)) {
                        estimoteMonitoring.startEstimoteMonitoring(new EstimoteMonitoringPacket(location.id.toHexString(), location.rssi, location.txPower, location.channel, location.timestamp));
                    }
                }
            }
        });
    }

    public void addNotification(String deviceId, String enterMessage, String exitMessage) {
        deviceIdBeacon = deviceId;
        enterMessages = enterMessage;
        exitMessages = exitMessage;

        URL_OFFER = Constants.URL_GET_OFFER;

        // Obtenemos valores de memoria interna
        settings = context.getSharedPreferences(DB, 0);

        strTipoSemento = settings.getString(Constants.TIPO_SEGMENTO, "");
        strTipoFidelizacion = settings.getString(Constants.TIPO_FIDELIZACION, "");
    }

    public void startMonitoring() {
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startLocationDiscovery();
            }
        });
    }


    /**
     * Async task class to get json by making HTTP call
     */
    private class GetOfferProduct extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {
            HttpHandler sh = new HttpHandler(context);
            Log.e("response-prmo", URL_OFFER+"-");
            return sh.makeServiceCall(URL_OFFER);
        }

        @Override
        protected void onPostExecute(String jsonStr) {
            super.onPostExecute(jsonStr);

            Log.e("response-prmo2", jsonStr+"-");

            if (jsonStr != null) {
                try {
                    JSONArray jsonArray;
                    jsonArray = new JSONArray(jsonStr);

                    if (jsonArray.length() > 0)
                        new Notification().Launch(context, new Random()+"", jsonStr);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
