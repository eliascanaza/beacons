package com.mcuhq.simplebluetooth.estimote.estimote;

import com.estimote.coresdk.cloud.model.Color;

public class EstimoteCloudBeaconDetails {

    private String beaconName;
    private String beaconId;

    public EstimoteCloudBeaconDetails(String beaconName, String beaconId) {
        this.beaconName = beaconName;
        this.beaconId = beaconId;
    }

    public String getBeaconName() {
        return beaconName;
    }

    public String getBeaconId() {
        return beaconId;
    }

    @Override
    public String toString() {
        return "[beaconName: " + getBeaconName() + ", beaconColor: " + getBeaconId() + "]";
    }
}
