package com.mcuhq.simplebluetooth.estimote.estimote;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.mcuhq.simplebluetooth.estimote.BeaconNotificationsManager;
import com.mcuhq.simplebluetooth.utils.Constants;
import com.mcuhq.simplebluetooth.utils.HttpHandler;
import com.mcuhq.simplebluetooth.utils.Notification;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;
import java.util.Random;

public class ProximityContentManager {

    private NearestBeaconManager nearestBeaconManager;
    private Context context;

    private Listener listener;
    private SharedPreferences settings;

    private String URL_OFFER = "";
    private String strTipoSemento = "";
    private String strTipoFidelizacion = "";
    private String DB = Constants.DB_NAME;

    public ProximityContentManager(Context context,
                                   List<String> deviceIDs,
                                   BeaconContentFactory beaconContentFactory) {
        final BeaconContentCache beaconContentCache = new BeaconContentCache(beaconContentFactory);

        URL_OFFER = Constants.URL_GET_OFFER;
        this.context = context;

        // Obtenemos valores de memoria interna
        settings = context.getSharedPreferences(DB, 0);

        strTipoSemento = settings.getString(Constants.TIPO_SEGMENTO, "");
        strTipoFidelizacion = settings.getString(Constants.TIPO_FIDELIZACION, "");

        nearestBeaconManager = new NearestBeaconManager(context, deviceIDs);
        nearestBeaconManager.setListener(new NearestBeaconManager.Listener() {
            @Override
            public void onNearestBeaconChanged(String deviceID) {
                if (listener == null) {
                    return;
                }

                if (deviceID != null) {
                    /*beaconContentCache.getContent(deviceID, new BeaconContentFactory.Callback() {
                        @Override
                        public void onContentReady(Object content) {
                            listener.onContentChanged(content);
                        }
                    });*/

                    //llamamos al servicio para obtener las ofertas
                    URL_OFFER = URL_OFFER.replace(Constants.COD_BEACON, deviceID);
                    URL_OFFER = URL_OFFER.replace(Constants.COD_PRODUCTO_FIDELIZACION, strTipoFidelizacion);
                    URL_OFFER = URL_OFFER.replace(Constants.COD_SEGMENTO_CLIENTE, strTipoSemento);
                    new GetOfferProduct().execute(deviceID);

                } else {
                    listener.onContentChanged(null);
                }
            }
        });

    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        void onContentChanged(Object content);
    }

    public void startContentUpdates() {
        nearestBeaconManager.startNearestBeaconUpdates();
    }

    public void stopContentUpdates() {
        nearestBeaconManager.stopNearestBeaconUpdates();
    }

    public void destroy() {
        nearestBeaconManager.destroy();
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetOfferProduct extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {
            HttpHandler sh = new HttpHandler(context);
            Log.e("response-prmo", URL_OFFER+"-");
            return sh.makeServiceCall(URL_OFFER);
        }

        @Override
        protected void onPostExecute(String jsonStr) {
            super.onPostExecute(jsonStr);

            Log.e("response-prmo2", jsonStr+"-");

            if (jsonStr != null) {
                try {
                    JSONArray jsonArray;
                    jsonArray = new JSONArray(jsonStr);

                    if (jsonArray.length() > 0)
                        new Notification().Launch(context, new Random()+"", jsonStr);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
