package com.mcuhq.simplebluetooth.model;

/**
 * Created by edgarcia on 12/09/2017.
 */

public class Product {
    String strProductName;
    String strCategoryName;
    String strBrand;
    String strDiscount;
    String strIdProduct;
    String strImagen;

    public Product(String strProductName,
                   String strCategoryName,
                   String strDiscount,
                   String strIdProduct,
                   String strImagen){
        this.strProductName = strProductName;
        this.strCategoryName = strCategoryName;
        this.strDiscount = strDiscount;
        this.strIdProduct = strIdProduct;
        this.strImagen = strImagen;
    }

    public String getStrProductName() {
        return strProductName;
    }

    public void setStrProductName(String strProductName) {
        this.strProductName = strProductName;
    }

    public String getStrCategoryName() {
        return strCategoryName;
    }

    public void setStrCategoryName(String strCategoryName) {
        this.strCategoryName = strCategoryName;
    }

    public String getStrImagen() {
        return strImagen;
    }

    public void setStrImagen(String strImagen) {
        this.strImagen = strImagen;
    }

    public String getStrBrand() {
        return strBrand;
    }

    public void setStrBrand(String strBrand) {
        this.strBrand = strBrand;
    }

    public String getStrDiscount() {
        return strDiscount;
    }

    public void setIntDiscount(int intDiscount) {
        this.strDiscount = strDiscount;
    }

    public String getStrIdProduct() {
        return strIdProduct;
    }

}

