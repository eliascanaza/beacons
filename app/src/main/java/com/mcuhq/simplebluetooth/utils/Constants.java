package com.mcuhq.simplebluetooth.utils;

/**
 * Created by eliascanaza on 13/10/17.
 */

public class Constants {
    public static final long TIME_SCAN = 10000;

    public static final String IP = "http://54.202.120.41:";

    public static final String LAST_RED_BLE = "LAST_BLUETOOTH";
    public static final String COD_BEACON_CONFIG = "beac";

    public static final String DB_NAME = "myDB_Promo";
    public static final String TIPO_FIDELIZACION = "TIPO_FIDELIZACION";
    public static final String TIPO_SEGMENTO = "TIPO_SEGMENTO";
    public static final String CLIENTE_BANCO = "CLIENTE_BANCO";

    public static final String COD_IMAGEN = "{IMAGEN}";
    public static final String COD_DOCUMENTO = "{DNI}";
    public static final String COD_BEACON = "{BEACON}";
    public static final String COD_PRODUCTO_FIDELIZACION = "{FIDELIZACION}";
    public static final String COD_SEGMENTO_CLIENTE = "{SEGMENTO}";

    public static final String VALUE_QR_KEY = "QR_VALUE";

    public static final String URL_GET_OFFER = IP+"8092/beacon/cobeac/{BEACON}/cofide/{FIDELIZACION}/cosegm/{SEGMENTO}";
    public static final String URL_GET_CUSTOMER_TYPE = IP+"8092/tipocliente/dni/{DNI}";
    public static final String URL_GET_IMAGEN = IP+"8089/getimage/imagen/{IMAGEN}";
    public static final String URL_BEACON_LEIDO = IP+"8086/beaconbileido/cobeac/{BEACON}/cofide/{FIDELIZACION}/cosegm/{SEGMENTO}";


}
