package com.mcuhq.simplebluetooth.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.mcuhq.simplebluetooth.OfferInterbank;
import com.mcuhq.simplebluetooth.R;

import java.util.Date;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by emamani on 12/09/2017.
 */

public class Notification {
    public void Launch(Context context, String title ,String jsonStr){

        NotificationCompat.Builder mBuilder;
        NotificationManager mNotifyMgr =(NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(context, OfferInterbank.class);
        notificationIntent.putExtra("jsonStr",jsonStr);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context.getApplicationContext())
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.interbank)
                .setContentTitle(title)
                .setContentText("OFERTAS!! - Solo clientes")
                .setVibrate(new long[] {100, 250, 100, 500})
                .setAutoCancel(true);

        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        mNotifyMgr.notify(m, mBuilder.build());
    }
}
