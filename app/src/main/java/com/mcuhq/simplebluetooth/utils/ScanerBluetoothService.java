package com.mcuhq.simplebluetooth.utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.widget.Toast;

/**
 * Created by emamani on 13/09/2017.
 */

public class ScanerBluetoothService extends Service {

    private ScanerTask scanerTask;
    private PowerManager.WakeLock wl;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        //this service will run until we stop it

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        /**
         * Lanza un timer para que en el servicio este leyendo la red de bluetooth aun asi que la app este cerrado
         * */
        scanerTask = new ScanerTask();
        scanerTask.launch( getApplicationContext());
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}