package com.mcuhq.simplebluetooth.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by edgarcia on 13/09/2017.
 */

public class ScanerTask {


    private final String TAG = ScanerTask.class.getSimpleName();
    private BluetoothAdapter mBTAdapter;
    private Context context;

    /**
     * Lanzamos un timer para scanear la red de bluetooth cada 10 segundos y lanzaremos una Notificacion
     * */
    public void launch (Context finalContext){

        context = finalContext;

        mBTAdapter = BluetoothAdapter.getDefaultAdapter(); // get a handle on the bluetooth radio

        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {
                            scannerBluetooth(context);
                        }
                        catch (Exception e) {
                            // TODO Auto-generated catch block
                            Toast.makeText(context,"[ERROR] Bluetooth NO soportado: "+e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, Constants.TIME_SCAN);
    }

    private void scannerBluetooth(Context context){
        // Check if the device is already discovering
        if(mBTAdapter.isDiscovering())
            mBTAdapter.cancelDiscovery();
        else{
            if(mBTAdapter.isEnabled()) {
                mBTAdapter.startDiscovery();
                //Toast.makeText(context, "Escaneando...", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
