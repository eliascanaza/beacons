package com.mcuhq.simplebluetooth.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.mcuhq.simplebluetooth.estimote.estimote.ProximityContentManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Random;

/**
 * Created by eliascanaza on 11/11/17.
 */

public class ValidateBeacon {
    private SharedPreferences settings;

    private String URL_OFFER = "";
    private String strTipoSemento = "";
    private String strTipoFidelizacion = "";
    private String DB = Constants.DB_NAME;

    private String regionIdentifier;

    private Context context;

    public ValidateBeacon(Context context){
        this.context = context;

        // Obtenemos valores de memoria interna
        settings = context.getSharedPreferences(DB, 0);
    }

    public void ExecuteService(String deviceID, String regionIdentifier){
        this.regionIdentifier =regionIdentifier;
        strTipoSemento = settings.getString(Constants.TIPO_SEGMENTO, "");
        strTipoFidelizacion = settings.getString(Constants.TIPO_FIDELIZACION, "");

        URL_OFFER = Constants.URL_GET_OFFER;
        URL_OFFER = URL_OFFER.replace(Constants.COD_BEACON, deviceID);
        URL_OFFER = URL_OFFER.replace(Constants.COD_PRODUCTO_FIDELIZACION, strTipoFidelizacion);
        URL_OFFER = URL_OFFER.replace(Constants.COD_SEGMENTO_CLIENTE, strTipoSemento);
        new GetOfferProduct().execute();
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetOfferProduct extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... url) {
            HttpHandler sh = new HttpHandler(context);
            Log.e("response-prmo", URL_OFFER+"-");
            return sh.makeServiceCall(URL_OFFER);
        }

        @Override
        protected void onPostExecute(String jsonStr) {
            super.onPostExecute(jsonStr);

            Log.e("response-prmo2", jsonStr+"-");

            if (jsonStr != null) {
                try {
                    JSONArray jsonArray;
                    jsonArray = new JSONArray(jsonStr);

                    if (jsonArray.length() > 0)
                        new Notification().Launch(context, regionIdentifier, jsonStr);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
